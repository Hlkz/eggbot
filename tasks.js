
const test = {
  name: 'test',
  loop: async task => {
    let date1 = new Date()
    await task.sendKey({ key: '1' })
    await task.wait(2000, true)
    await task.sendKey({ key: '2' })
    await task.wait(2000)
    let date2 = new Date()
    console.log('time', date1.getTime() - date2.getTime())
  },
  totalDelay: 4000,
}

//
//
//

let delay_buy = 1000
let delay_place = 1800
let delay_collect = 2500
let delay_sell = 250
let totalDelay = delay_buy + delay_place + delay_collect + delay_sell

const multiple = {
  name: 'multiple',
  loop: async task => {
    let date1 = new Date()
  
    await task.moveTo({ winId: 1 })
    await task.sendKey({ key: '3', winId: 1 })
    await task.sendKey({ key: '1', winId: 1 })
    await task.wait(delay_buy, true)

    await task.sendKey({ key: '2', winId: 1 })
    await task.wait(delay_place)

    await task.rightclick({ winId: 1 })
    await task.rightclick({ winId: 2 })
    await task.rightclick({ winId: 3 })
    await task.wait(delay_collect)

    await task.click({ diffX: 59, diffY: -43, winId: 1 })
    await task.sendKey({ key: '3', winId: 2 })
    await task.click({ diffX: 59, diffY: -43, winId: 2 })
    await task.sendKey({ key: '3', winId: 3 })
    await task.click({ diffX: 59, diffY: -43, winId: 3 })
    await task.wait(delay_sell)

    let date2 = new Date()
    console.log('time', date1.getTime() - date2.getTime())
  },
  totalDelay,
  totalDelay: 1000,
}

const poser = {
  name: 'poser',
  loop: async task => {
    let date1 = new Date()
    await task.sendKey({ key: '1' })
    await task.wait(delay_buy, true)
    await task.moveTo()
    await task.sendKey({ key: '2' })
    await task.wait(delay_place)
    await task.rightclick()
    await task.wait(delay_collect)
    await task.click({ diffX: 59, diffY: -43 })
    await task.moveTo()
    await task.sendKey({ key: '3' })
    await task.wait(delay_sell)
    let date2 = new Date()
    console.log('time', date1.getTime() - date2.getTime())
  },
  totalDelay,
  totalDelay: 1000,
}

const ramasser = {
  name: 'ramasser',
  loop: async task => {
    let date1 = new Date()
    await task.rightclick()
    await task.wait(delay_collect)
    await task.click({ diffX: 59, diffY: -43 })
    await task.sendKey({ key: '3' })
    await task.wait(delay_sell)
    await task.wait(delay_buy)
    await task.wait(delay_place, true)
    let date2 = new Date()
    console.log('time', date1.getTime() - date2.getTime())
  },
  totalDelay,
  totalDelay: 1000,
}

module.exports = {
  test,
  multiple,
  poser,
  ramasser,
}
