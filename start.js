const { startTask } = require('./task')
const { test, multiple, poser, ramasser } = require('./tasks')
const { resetCurHandle, getWinInfos } = require('./win')

const arg2 = process.argv[2]
console.log('arg2', arg2)

function start() {
  resetCurHandle()
  return startTask(multiple)

  if (arg2 == 'all') {
    let winInfos = getWinInfos()
    winInfos.forEach(winInfo => {
      const taskTemplate = winInfo.id == 1 ? poser : ramasser
      startTask({ ...taskTemplate, winInfo })
    })
    return
  }
  const taskTemplate = arg2 == 'test' ? test : arg2 == 'pose' ? poser : ramasser
  startTask(taskTemplate)
}


module.exports = {
  start,
}
