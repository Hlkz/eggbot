const { Hardware, Virtual, GlobalHotkey } = require('keysender')

const wait = ms => new Promise(s => setTimeout(s, ms))

function kill() { return new Promise(s => 1) }

function exit() {
  process.exit(0)
}

function handleError(err) {
  console.log('Error:', err)
}

function getNowMS() {
  return (new Date()).getTime()
}

//
// keysender
//

const handle = new Virtual(null, 'Test')
const ghw = new Hardware(handle)
const getGenericHardware = () => ghw

function createHardware(handle) {
  if (!handle) handle = new Virtual(null, 'Test')
  const hw = new Hardware(handle)
  return hw
}

function registerHotkey(key, fn) {
  new GlobalHotkey({ key, mode: 'once', action: () => Promise.resolve(fn()).catch(handleError) })
}

async function sendKey(key) {
  await ghw.keyboard.sendKey(key)
}

async function moveTo(x, y) {
  await ghw.mouse.moveTo(x, y)
}

async function click(...args) {
  await ghw.mouse.click(...args)
}

async function rightclick(...args) {
  await ghw.mouse.click('right', ...args)
}

function getPos() {
  return ghw.mouse.getPos()
}

function logPos() {
  console.log(getPos())
}

function getSenderFns(handle) {
  if (!handle) handle = new Virtual(null, 'Test')
  const obj = new Hardware(handle)

  async function sendKey(key) {
    await obj.keyboard.sendKey(key)
  }

  async function moveTo(x, y) {
    await obj.mouse.moveTo(x, y)
  }

  async function click(...args) {
    await obj.mouse.click(...args)
  }

  async function rightclick(...args) {
    await obj.mouse.click('right', ...args)
  }

  function getPos() {
    return obj.mouse.getPos()
  }

  return {
    sendKey,
    moveTo,
    click,
    rightclick,
    getPos,
  }
}

module.exports = {
  wait,
  kill,
  exit,
  handleError,
  getNowMS,
  //
  registerHotkey,
  createHardware,
  getGenericHardware,
  sendKey,
  moveTo,
  click,
  rightclick,
  getPos,
  logPos,
  getSenderFns,
}
