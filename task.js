const {
  wait,
  kill,
  sendKey: _sendKey,
  handleError,
  getNowMS,
  //
  getSenderFns,
} = require('./util')
const { swapWinIfNeeded, getWinInfo } = require('./win')
const { createTasktime } = require('./tasktime')

let DIFFDELAY = 2850

let lastId = 0
let tasks = []
const getRunningTasks = () => tasks.filter(e => e.isRunning)

function startTask(options) {
  let { loop: loopfn, totalDelay, winInfo } = options
  const winId = winInfo?.id || null
  const handle = winInfo?.handle || null

  if (!loopfn) throw Error('Missing loopfn')
  if (!totalDelay) throw Error('Missing totalDelay')

  console.log('create task', options)

  const {
    sendKey: _sendKey,
    getPos,
  } = getSenderFns(handle)

  const id = ++lastId
  let isRunning = false
  let pos = winId ? winInfo.pos : getPos()
  let startX = pos.x
  let startY = pos.y
  let tasktime = createTasktime()

  const task = { id, isRunning }

  function setRunning(value) {
    isRunning = task.isRunning = value
  }

  async function start() {
    setRunning(true)
    await waitBeforeStart()
    while(isRunning) await loopfn(task)
  }

  function stopRunning() {
    setRunning(false)
  }


  async function waitBeforeStart() {
    let nowMS = getNowMS()
    let count = Math.ceil(nowMS / totalDelay)
    let nextStart = count * totalDelay
    let delay = nextStart - nowMS + ((winId || 0) * DIFFDELAY)
    console.log((winId?winId+': ':'')+'waiting', delay, 'before start')
    await wait(delay)
    tasktime.init()
  }


  async function sendKey(key, _winId) {
    _winId = _winId || winId
    const handle = _winId && getWinInfo(_winId)?.handle || null
    if (handle) await swapWinIfNeeded(handle)
    await _sendKey(key).catch(handleError)
  }

  async function moveTo(options) {
    let { x, y, diffX, diffY, isRight, winId: _winId } = options || {}
    const { move: ffimove } = require('./ffi')

    _winId = _winId || winId
    const winInfo = _winId && getWinInfo(_winId)
    const baseX = winInfo?.pos ? winInfo.pos.x : startX
    const baseY = winInfo?.pos ? winInfo.pos.y : startY
    let finalX = x || baseX + (diffX || 0)
    let finalY = y || baseY + (diffY || 0)

    // if (winId) await swapWinIfNeeded(handle)
    ffimove(finalX, finalY)
  }

  async function click(options) {
    const { click: fficlick } = require('./ffi')
    let { x, y, diffX, diffY, isRight, winId: _winId } = options || {}

    _winId = _winId || winId
    const winInfo = _winId && getWinInfo(_winId)
    const baseX = winInfo?.pos ? winInfo.pos.x : startX
    const baseY = winInfo?.pos ? winInfo.pos.y : startY
    let finalX = x || baseX + (diffX || 0)
    let finalY = y || baseY + (diffY || 0)
    const handle = winInfo?.handle || null

    if (handle) await swapWinIfNeeded(handle)
    fficlick(isRight ? 'right' : 'left', finalX, finalY)
  }

  async function rightclick(options) {
    options = options || {}
    await click({ ...options, isRight: true })
  }

  async function _wait(ms) {
    if (!isRunning) return kill()
    await tasktime.wait(ms)
    if (!isRunning) return kill()
  }

  task.stopRunning = stopRunning
  //
  task.wait = _wait
  task.setDelayMod = tasktime.setDelayMod
  //
  task.sendKey = sendKey
  task.moveTo = moveTo
  task.click = click
  task.rightclick = rightclick

  tasks.push(task)

  start().catch(handleError)
}

function stopRunning() {
  let tasks = getRunningTasks()
  tasks.forEach(task => task.stopRunning())
}

function setDelayMod(delay) {
  let task = getRunningTasks()
  tasks.forEach(task => task.setDelayMod(delay))
}

module.exports = {
  startTask,
  getRunningTasks,
  stopRunning,
  setDelayMod,
}
