const { wait, getNowMS } = require('./util')

function createTasktime() {

  let lastMS = getNowMS()
  let nextMS
  let delayMod = null

  function init() {
    lastMS = getNowMS()
  }

  function setDelayMod(ms) {
    delayMod = ms
  }

  async function _wait(delay, canBeModified) {
    if (canBeModified && delayMod) {
      delay += delayMod
      delayMod = null
    }

    // Réguler le delay sur l'heure
    let nowMS = getNowMS()
    nextMS = lastMS + delay
    delay = nextMS - nowMS
    let delayDiff = delay
    delay = Math.max(0, delay)
    // console.log('delayDiff', delayDiff, 'delay', delay)

    await wait(delay)
    lastMS = nextMS
  }

  const tasktime = {
    init,
    wait: _wait,
    setDelayMod,
  }
  return tasktime
}

module.exports = {
  createTasktime,
}