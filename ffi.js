const ffi = require('ffi-napi')

const MOUSEEVENTF_LEFTDOWN = 2
const MOUSEEVENTF_LEFTUP = 4
const MOUSEEVENTF_RIGHTDOWN = 8
const MOUSEEVENTF_RIGHTUP = 16

const user32 = new ffi.Library('user32', {
  // focus
  'GetTopWindow': ['long', ['long']],
  'FindWindowA': ['long', ['string', 'string']],
  'SetActiveWindow': ['long', ['long']],
  'SetForegroundWindow': ['bool', ['long']],
  'BringWindowToTop': ['bool', ['long']],
  'ShowWindow': ['bool', ['long', 'int']],
  'SwitchToThisWindow': ['void', ['long', 'bool']],
  'GetForegroundWindow': ['long', []],
  'AttachThreadInput': ['bool', ['int', 'long', 'bool']],
  'GetWindowThreadProcessId': ['int', ['long', 'int']],
  'SetWindowPos': ['bool', ['long', 'long', 'int', 'int', 'int', 'int', 'uint']],
  'SetFocus': ['long', ['long']],
  // get
  'GetCursorPos': ['bool', ['pointer']],
  'GetWindowRect': ['bool', ['int', 'pointer']],
  // click
  'SetCursorPos': ['long', ['long', 'long']],
  'mouse_event': ['void', ['int', 'int', 'int', 'int', 'int']]
})

const kernel32 = new ffi.Library('Kernel32.dll', {
  'GetCurrentThreadId': ['int', []]
})

//
// Misc util
//

let repbuffer = new Buffer(16)

function pointerToRect(rectPointer) {
  const left = rectPointer.readInt16LE(0)
  const top = rectPointer.readInt16LE(4)
  const right = rectPointer.readInt16LE(8)
  const bottom = rectPointer.readInt16LE(12)
  const rect = {
    left,
    top,
    right,
    bottom,
  }
  return rect
}

//
//
//

function focusWindow(handle) {
  // const handle = user32.FindWindowA(null, "World of Warcraft")
  const foregroundHWnd = user32.GetForegroundWindow()
  const currentThreadId = kernel32.GetCurrentThreadId()
  const windowThreadProcessId = user32.GetWindowThreadProcessId(foregroundHWnd, null)
  const showWindow = user32.ShowWindow(handle, 9)
  const setWindowPos1 = user32.SetWindowPos(handle, -1, 0, 0, 0, 0, 3)
  const setWindowPos2 = user32.SetWindowPos(handle, -2, 0, 0, 0, 0, 3)
  const setForegroundWindow = user32.SetForegroundWindow(handle)
  const attachThreadInput = user32.AttachThreadInput(windowThreadProcessId, currentThreadId, 0)
  const setFocus = user32.SetFocus(handle)
  const setActiveWindow = user32.SetActiveWindow(handle)
}

function move(x, y) {
  user32.SetCursorPos(x, y)
}

function click(type, x, y) {
  user32.SetCursorPos(x, y)
  if (type == 'right') {
    user32.mouse_event(MOUSEEVENTF_RIGHTDOWN, 0 ,0 ,0 ,0)
    user32.mouse_event(MOUSEEVENTF_RIGHTUP, 0, 0, 0, 0)
  } else {
    user32.mouse_event(MOUSEEVENTF_LEFTDOWN, 0 ,0 ,0 ,0)
    user32.mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0)
  }
}

function getCurrentWindow() {
  return user32.GetForegroundWindow()
}

function getMousePos() {
  const p = user32.GetCursorPos(repbuffer)
  const x = repbuffer[0] + (repbuffer[1] * 256)
  const y = repbuffer[4] + (repbuffer[5] * 256)
  return { x, y }
}

function getWindowRect(handle) {
  const rectPointer = Buffer.alloc(16)
  const res = user32.GetWindowRect(handle, rectPointer)
  return !res ? null : pointerToRect(rectPointer)
}


module.exports = {
  focusWindow,
  move,
  click,
  getCurrentWindow,
  getMousePos,
  getWindowRect,
}
