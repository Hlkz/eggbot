const { wait } = require('./util')
const { focusWindow, getCurrentWindow, getMousePos, getWindowRect } = require('./ffi')

let curHandle = null
const resetCurHandle = () => { curHandle = null }

async function swapWinIfNeeded(handle) {
  handle = handle || null
  if (curHandle == handle) return
  focusWindow(handle)
  await wait(15)
}

//
// Window info
//

let winInfo = []
const setWinInfo = index => {
  let handle = getCurrentWindow()
  winInfo[index] = {
    handle,
    rect: getWindowRect(handle),
    pos: getMousePos(),
  }
  console.log(index, winInfo[index])
}
const resetWinInfo = () => {
  winInfo = []
}
const getWinInfo = index => winInfo[index]
const getWinInfos = () => winInfo.map((obj, id) => ({ id, ...obj })).filter(e => e.id && e.handle)


module.exports = {
  // initWins,
  resetCurHandle,
  swapWinIfNeeded,
  //
  setWinInfo,
  getWinInfo,
  getWinInfos,
  resetWinInfo,
}
