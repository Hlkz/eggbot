const { start } = require('./start')
const { stopRunning, setDelayMod } = require('./task')
const { registerHotkey, exit, logPos } = require('./util')
const { setWinInfo, resetWinInfo } = require('./win')

registerHotkey('f1', exit)
registerHotkey('f2', start)
registerHotkey('f3', stopRunning)

registerHotkey('f4', () => setDelayMod(-50))
registerHotkey('f5', () => setDelayMod(50))

registerHotkey('f6', logPos)

registerHotkey('num1', () => setWinInfo(1))
registerHotkey('num2', () => setWinInfo(2))
registerHotkey('num3', () => setWinInfo(3))
registerHotkey('num4', () => setWinInfo(4))
registerHotkey('num5', () => setWinInfo(5))
registerHotkey('num0', () => resetWinInfo())
